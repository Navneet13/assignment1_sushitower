//
//  InterfaceController.swift
//  Assignment1_SushiTower WatchKit Extension
//
//  Created by navneet on 2019-11-01.
//  Copyright © 2019 navneet. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var tapGestureLabel: WKInterfaceLabel!
    @IBOutlet weak var leftButton: WKInterfaceButton!
    @IBOutlet weak var rightButton: WKInterfaceButton!
    
    @IBOutlet weak var addTimerButton: WKInterfaceButton!
    @IBOutlet weak var timerWarningLabel: WKInterfaceLabel!
    
    var pause = false
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if WCSession.isSupported() {
            let wcsession = WCSession.default
            wcsession.delegate = self
            wcsession.activate()
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func leftButtonTapped() {
        moveCat(direction: "left")
    }
    
    @IBAction func rightButtonTapped() {
        moveCat(direction: "right")
    }
    
    @IBAction func longPress(_ sender: Any) {
        
        if let _ = sender as? WKTapGestureRecognizer {
            
            if pause {
                tapGestureLabel.setText("Tap here to Pause")
                pause = false
            } else {
                tapGestureLabel.setText("Tap here to Resume")
                pause = true
            }
            
            if WCSession.default.isReachable {
                WCSession.default.sendMessage(
                    ["pause" : pause],
                    replyHandler: {
                        (_ replyMessage: [String: Any]) in
                }, errorHandler: { (error) in
                })
            }
            else {
                print("Phone is not reachable")
            }
        }
    }
    
    @IBAction func addTimerButtonPressed() {
        
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(
                ["MoreTime" : true],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    print("Message sent, put something here if you are expecting a reply from the phone")
            }, errorHandler: { (error) in
                print("Error while sending message: \(error)")
            })
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    func moveCat(direction: String) {
        
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(
                ["ButtonMove" : direction],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
            }, errorHandler: { (error) in
            })
        }
        else {
            print("Phone is not reachable")
        }
    }
    
    func gameOver() {
        self.leftButton.setEnabled(false)
        self.rightButton.setEnabled(false)
    }
    
    func askForName() {
        // Enter name and send the score to cloud firebase
        presentTextInputController(withSuggestions: [""], allowedInputMode:   WKTextInputMode.plain) { (arr: [Any]?) in
            print(arr ?? "Not find")
            self.sendHighScore(name: arr?[0] as! String)
        }
    }
    
    func sendHighScore(name: String) {
        
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(
                ["HighScoreName" : name],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
            }, errorHandler: { (error) in
            })
        }
        else {
            print("Phone is not reachable")
        }
    }
}

extension InterfaceController: WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        // Timer Warning Message - 5, 10, 15 seconds
        if let timer = message["Timer"] as? String {
            self.timerWarningLabel.setText(timer)
        } else {
            self.timerWarningLabel.setText("")
        }
        
        // Game Over
        if let gameOverMessage = message["Over"] as? String {
            self.timerWarningLabel.setText(gameOverMessage)
            self.gameOver()
            
            if let isHighScore = message["HighScore"] as? Bool, isHighScore {
                askForName()
            }
        }
        
        // Power Ups
        if (message["PowerUp"] as? Bool) != nil {
            self.addTimerButton.setHidden(false)
        } else {
            self.addTimerButton.setHidden(true)
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
    }
}
