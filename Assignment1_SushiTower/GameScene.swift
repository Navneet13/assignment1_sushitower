//
//  GameScene.swift
//  Assignment1_SushiTower
//
//  Created by navneet on 2019-11-01.
//  Copyright © 2019 navneet. All rights reserved.
//

import SpriteKit
import GameplayKit
import Firebase

enum CatMoveDirection: String {
    
    case EMPTY
    case LEFT = "left"
    case RIGHT = "right"
}

class GameScene: SKScene {
    
    var viewController: GameViewController!
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    
    // Make a tower
    var sushiTower:[SushiPiece] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var catPosition: CatMoveDirection = .LEFT
    
    // Show life and score labels
    let lifeLabel = SKLabelNode(text:"Lives: ")
    let scoreLabel = SKLabelNode(text:"Score: ")
    let timerLabel = SKLabelNode(text:"Timer: ")
    let highScoreLabel = SKLabelNode(text:"High Score: ")
    
    let gameOverLabel = SKLabelNode(text:"Game Over!!")
    
    var lives = 5
    var score = 0
    var highScore = 0
    var timerValue = 25
    
    var powerUpsRemaining = 2
    var powerUpTime1 = Int.random(in: 1 ..< 15)
    var powerUpTime2 = Int.random(in: 16 ..< 25)
    
    var showPowerUpTime = 0
    var activatePowerUp = false
    
    var isGameOver = false
    var isGamePaused = false
    
    func spawnSushi() {
        
        // -----------------------
        // MARK: PART 1: ADD SUSHI TO GAME
        // -----------------------
        
        // 1. Make a sushi
        let sushi = SushiPiece(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
    }
    
    override func didMove(to view: SKView) {
        
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x: self.size.width * 0.25, y: 100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x: self.size.width * 0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        
        // High Score label
        self.highScoreLabel.position.x = 10
        self.highScoreLabel.position.y = size.height - 40
        self.highScoreLabel.fontName = "Avenir"
        self.highScoreLabel.fontSize = 20
        self.highScoreLabel.horizontalAlignmentMode = .left
        self.highScoreLabel.zPosition = 100
        addChild(highScoreLabel)
        
        // Score Label
        self.scoreLabel.position.x = 10
        self.scoreLabel.position.y = size.height - 80
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.fontSize = 20
        self.scoreLabel.horizontalAlignmentMode = .left
        self.scoreLabel.zPosition = 100
        addChild(scoreLabel)
        
        // Life label
        self.lifeLabel.position.x = 10
        self.lifeLabel.position.y = size.height - 120
        self.lifeLabel.fontName = "Avenir"
        self.lifeLabel.fontSize = 20
        self.lifeLabel.text = "Lives: \(lives)"
        self.lifeLabel.horizontalAlignmentMode = .left
        self.lifeLabel.zPosition = 100
        addChild(lifeLabel)
        
        // Timer Label
        self.timerLabel.position = CGPoint(x: 10, y: size.height - 160)
        self.timerLabel.fontName = "Avenir"
        self.timerLabel.fontSize = 20
        self.timerLabel.horizontalAlignmentMode = .left
        self.timerLabel.zPosition = 100
        addChild(self.timerLabel)
        
        run(SKAction.repeatForever(SKAction.sequence([
            SKAction.run({ [unowned self] in
                if self.timerValue > 0 {
                    self.timerValue = self.timerValue - 1
                    
                    // Update PowerUps
                    self.updatePowerUp()
                    
                    // SHow Timer Alert - 5,10,15 seconds
                    if self.timerValue == 15 || self.timerValue == 10 || self.timerValue == 5 {
                        self.sendTimerMessageToWatch(message: "\(self.timerValue) seconds left")
                    } else {
                        self.sendTimerMessageToWatch(message: "")
                    }
                } else {
                    self.gameOver()
                }
                
                self.timerLabel.text = "Time left: \(self.timerValue)"
            }),
            SKAction.wait(forDuration: 1)
            ])), withKey: "countdown")
    }
    
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if !isGameOver && !isGamePaused {
            
            
            // This is the shortcut way of saying:
            //      let mousePosition = touches.first?.location
            //      if (mousePosition == nil) { return }
            guard let mousePosition = touches.first?.location(in: self) else {
                return
            }
            
            print(mousePosition)
            
            // ------------------------------------
            // MARK: UPDATE THE SUSHI TOWER GRAPHICS
            //  When person taps mouse,
            //  remove a piece from the tower & redraw the tower
            // -------------------------------------
            let pieceToRemove = self.sushiTower.first
            if (pieceToRemove != nil) {
                // SUSHI: hide it from the screen & remove from game logic
                pieceToRemove!.removeFromParent()
                self.sushiTower.remove(at: 0)
                
                // SUSHI: loop through the remaining pieces and redraw the Tower
                for piece in sushiTower {
                    piece.position.y = piece.position.y - SUSHI_PIECE_GAP
                }
                
                // To make the tower inifnite, then ADD a new piece
                self.spawnSushi()
            }
            
            // ------------------------------------
            // MARK: SWAP THE LEFT & RIGHT POSITION OF THE CAT
            //  If person taps left side, then move cat left
            //  If person taps right side, move cat right
            // -------------------------------------
            // 1. Detect where person clicked
            let middleOfScreen  = self.size.width / 2
            if (mousePosition.x < middleOfScreen) {
                print("TAP LEFT")
                updatePositions(direction: .LEFT)
            }
            else {
                print("TAP RIGHT")
                updatePositions(direction: .RIGHT)
            }
            
            punchAnimation()
            winOrLooseCondition()
        }
    }
    
    // ------------------------------------
    // MARK: UPDATE THE SUSHI TOWER GRAPHICS
    //  When person taps mouse,
    //  remove a piece from the tower & redraw the tower
    // -------------------------------------
    func updateSushiTower() {
        
        let pieceToRemove = self.sushiTower.first
        if (pieceToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // To make the tower inifnite, then ADD a new piece
            self.spawnSushi()
        }
    }
    
    // ------------------------------------
    // MARK: UPDATE Cat Position to Left or Right
    // ------------------------------------
    func updatePositions(direction: CatMoveDirection) {
        
        if !isGamePaused && !isGameOver {
            updateSushiTower()
            catPosition = direction
            
            if direction == .LEFT {
                cat.position = CGPoint(x: self.size.width * 0.25, y: 100)
                
                // Change the cat's direction
                let facingRight = SKAction.scaleX(to: 1, duration: 0)
                self.cat.run(facingRight)
            } else if direction == .RIGHT {
                
                cat.position = CGPoint(x: self.size.width*0.85, y: 100)
                
                // change the cat's direction
                let facingLeft = SKAction.scaleX(to: -1, duration: 0)
                self.cat.run(facingLeft)
            }
            
            punchAnimation()
            winOrLooseCondition()
        }
    }
    
    // ------------------------------------
    // MARK: ANIMATION OF PUNCHING CAT
    // -------------------------------------
    func punchAnimation() {
        
        // Show animation of cat punching tower
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
    }
    
    // ------------------------------------
    // MARK: WIN AND LOSE CONDITIONS
    // ------------------------------------
    func winOrLooseCondition() {
        
        if (self.sushiTower.count > 0) {
            // 1. if CAT and STICK are on same side - OKAY, keep going
            // 2. if CAT and STICK are on opposite sides -- YOU LOSE
            let firstSushi: SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
            
            if (catPosition == chopstickPosition) {
                // cat = left && chopstick == left
                // cat == right && chopstick == right
                updateLives()
            }
            else if (catPosition != chopstickPosition) {
                // cat == left && chopstick = right
                // cat == right && chopstick = left
                self.score = self.score + 10
                self.scoreLabel.text = "Score: \(self.score)"
            }
        }
            
        else {
            print("Sushi tower is empty!")
        }
    }
    
    // ------------------------------------
    // MARK: ADD TIMER
    // ------------------------------------
    func addTime(seconds: Int) {
        self.timerValue = self.timerValue + seconds
    }
    
    func updateLives() {
        if self.lives > 0 {
            self.lives = self.lives - 1
            self.lifeLabel.text = "Lives: \(self.lives)"
        } else {
            self.gameOver()
        }
    }
    
    func updatePowerUp() {
        
        // Show Power ups
        if self.timerValue == self.powerUpTime1 || self.timerValue == self.powerUpTime2 {
            self.showPowerUpOnWatch(powerUp: true)
            self.activatePowerUp = true
        }
        
        if self.activatePowerUp {
            self.showPowerUpTime = self.showPowerUpTime + 1
        }
        
        if self.showPowerUpTime >= 2 {
            self.activatePowerUp = false
            self.showPowerUpTime = 0
            self.showPowerUpOnWatch(powerUp: false)
        }
    }
    
    func pause(pause: Bool) {
        
        if let action = self.action(forKey: "countdown") {
            if pause {
                isGamePaused = true
                action.speed = 0
            } else {
                isGamePaused = false
                action.speed = 1
            }
        }
    }
    
    func gameOver() {
        isGameOver = true
        self.removeAction(forKey: "countdown")
        self.sendGameOverMessageToWatch()
        
        // Life label
        self.gameOverLabel.position.x = size.width / 2
        self.gameOverLabel.position.y = size.height / 2
        self.gameOverLabel.fontName = "Avenir"
        self.gameOverLabel.fontSize = 40
        self.gameOverLabel.text = "GAME OVER !!!!"
        self.gameOverLabel.horizontalAlignmentMode = .center
        self.gameOverLabel.zPosition = 100
        
        addChild(self.gameOverLabel)
    }
    
    func sendTimerMessageToWatch(message: String) {
        self.viewController.sendMessageToWatch(messageDictionary: ["Timer": message])
    }
    
    func sendGameOverMessageToWatch() {
        
        var dictionary = [String: Any]()
        dictionary["Over"] = "Game Over"
        
        if isHighScore() {
            dictionary["HighScore"] = true
        }
        self.viewController.sendMessageToWatch(messageDictionary: dictionary)
    }
    
    func isHighScore() -> Bool {
        if self.score > self.highScore {
            return true
        }
        return false
    }
    
    func showPowerUpOnWatch(powerUp: DarwinBoolean) {
        self.viewController.sendMessageToWatch(messageDictionary: ["PowerUp": true])
    }
}
