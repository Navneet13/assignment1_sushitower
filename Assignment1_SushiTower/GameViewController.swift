//
//  ViewController.swift
//  Assignment1_SushiTower
//
//  Created by navneet on 2019-11-01.
//  Copyright © 2019 navneet. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import WatchConnectivity
import Firebase

class GameViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var goButton: UIButton!
    var scene: GameScene!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        else {
            print("Phone does not support WCSession")
        }
    }
    
    @IBAction func goButtonPressed(_ sender: Any) {
        
        guard let name =  nameTextField.text, !name.isEmpty else {
            showMessage(message: "Please enter name first.")
            return
        }
        fetchHighScore(name: name)
    }
    
    func showMessage(message: String) {
        
        let alertController = UIAlertController.init(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(action)
        
        self.present(alertController, animated: false, completion: nil)
    }
    
    func initializeGameScene(highScore: Int) {
        
        nameTextField.isHidden = true
        goButton.isHidden = true
        
        scene = GameScene(size: self.view.bounds.size)
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        // property to show hitboxes
        skView.showsPhysics = true
        scene.scaleMode = .aspectFill
        scene.viewController = self
        self.scene.highScore = highScore
        self.scene.highScoreLabel.text = "High Score: \(highScore)"
        skView.presentScene(scene)
    }
}

extension GameViewController: WCSessionDelegate {
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        // 1. When a message is received from the watch, output a message to the UI
        // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
        // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
        DispatchQueue.main.async {
            print("\nMessage Received: \(message)")
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
        
        if let moveDirection = message["ButtonMove"] as? String {
            if moveDirection == CatMoveDirection.LEFT.rawValue {
                scene.updatePositions(direction: .LEFT)
            } else if moveDirection == CatMoveDirection.RIGHT.rawValue {
                scene.updatePositions(direction: .RIGHT)
            }
        }
        
        if let moreTimer = message["MoreTime"] as? Bool, moreTimer {
            scene.addTime(seconds: 10)
        }
        
        if let pause = message["pause"] as? Bool {
            scene.pause(pause: pause)
        }
        
        if let highScoreName = message["HighScoreName"] as? String {
            self.saveHighScore(name: highScoreName)
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("I am here in the activation state")
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func sendMessageToWatch(messageDictionary: [String: Any]) {
        
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(messageDictionary, replyHandler: nil)
        }
        else {
            print("Watch is not reachable")
        }
    }
}

extension GameViewController {
    
    func saveHighScore(name: String) {
        
        let ref: DatabaseReference! = Database.database().reference()
        let highScore = scene.score
        ref.child(name).setValue(highScore)
    }
    
    func fetchHighScore(name: String) {
        if !name.isEmpty {
            let ref: DatabaseReference! = Database.database().reference()
            ref.child(name).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? Int
                self.initializeGameScene(highScore: value ?? 0)
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }
}
